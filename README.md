# GitLab Dark Theme

A WIP dark theme for the website [GitLab.com](https://gitlab.com/maxigaz/gitlab-dark). 

This style is written in the [usercss](https://github.com/openstyles/stylus/wiki/Usercss) format, specifically for [Stylus](https://add0n.com/stylus.html), although it might be compatible with other style managers such as [xStyle](https://github.com/FirefoxBar/xStyle).

## Install with Stylus

If you have Stylus installed, click on the banner below and a new window of Stylus should open, asking you to confirm to install the style.

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/maxigaz/gitlab-dark/raw/master/gitlab-dark.user.css)  

## Screenshot

![A screenshot showing the main page of a project](Screenshot1.png)

## Known issue

The navigation bar on the top of the page may have a different background colour and some items in the left sidebar may appear too dark to be legible. (See the first screenshot in [this issue](https://gitlab.com/maxigaz/gitlab-dark/issues/21) for reference.) The reason for this is that different built-in navigation themes have different CSS selectors, and this userstyle only covers the default theme (Indigo).

So, if this happens, go to User Settings → [Preferences](https://gitlab.com/profile/preferences), and change the Navigation theme to "Indigo".

## Credits

The userstyle is inspired by [Arc Dark](https://github.com/horst3180/Arc-theme), although it doesn't follow all its design choices.

The logo of the project is based on [the official logo of GitLab](https://about.gitlab.com/press/), licenced under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Licence](https://creativecommons.org/licenses/by-nc-sa/4.0/).

## More userstyles from me

If you like this userstyle, check out the list of all styles created/maintained by me [here](https://gitlab.com/maxigaz/userstyles).
